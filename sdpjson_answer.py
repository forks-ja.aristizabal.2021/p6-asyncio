import json
import asyncio
import sdp_transform
class EchoServerProtocol:
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = data.decode()
        print('Received %r from %s' % (message, addr))
        print()
        json_data = json.loads(message)
        sdpData= json_data['sdp']   # Get the SDP data
        answer = sdp_transform.parse(sdpData)   # convert SDP data to dictionary
        answer['media'][0]['port'] = 34543
        answer['media'][1]['port'] = 34543
        sdpData = sdp_transform.write(answer)   # convert SDP modified dictionary to string
        jsonAnswer = {
            "type": "answer",
            "sdp": sdpData}     # transform sdp answer text to json
        print('Send %r to %s' % (jsonAnswer, addr))
        data = json.dumps(jsonAnswer).encode()  
        self.transport.sendto(data, addr)


async def main():
    print("Starting UDP server")

    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # One protocol instance will be created to serve all
    # client requests.
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoServerProtocol(),
        local_addr=('127.0.0.1', 9999))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()

if __name__ == "__main__":
    asyncio.run(main())